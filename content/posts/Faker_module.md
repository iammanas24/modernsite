---
title: "Faker Module"
date: 2021-03-19
image: "images/blog/django.png"
description: "This is meta description."
draft: false
---

# Faker Module

Create a Project Named : FakerModule
Create an App : FakeApp

### Install Following Packages


- Django==3.1.7
- Faker==6.6.2
- mysqlclient==2.0.3



models.py
---------

```
from django.db import models

class FakeData(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    job = models.CharField(max_length=100)
    company = models.CharField(max_length=100)
    salary = models.IntegerField()
    city = models.CharField(max_length=100)
    dob = models.DateField(max_length=100)
    address = models.CharField(max_length=100)

    def __str__(self):
        return self.first_name
```


views.py
--------
```
from django.shortcuts import render
from Fakeapp.models import FakeData
from django.http.response import HttpResponse
import faker
fake = faker.Faker()

def fake_view(request):
    for i in range(2000):
        first_name = fake.first_name()
        last_name = fake.last_name()
        email = fake.email()
        job = fake.random_element(elements=('HR', 
        'TL', 'PM', 'Admin'))
        company = fake.random_element(
            elements=('Micropyramid', 'DjangoGirls', 
            'TCS', 'Infosys', 'Deloitte'))
        salary = fake.random_element(
            elements=(10000, 15000, 20000, 25000, 30000))
        city = fake.random_element(
            elements=('Bbsr', 'Hyd', 'Bang', 'puna', 'patna'))
        dob = fake.date_time()
        address = fake.address()

        data = FakeData(
            first_name=first_name,
            last_name=last_name,
            email=email,
            job=job,
            company=company,
            salary=salary,
            city=city,
            dob=dob,
            address=address
        )
        data.save()
    return HttpResponse('Ojacers Data Inserted')
```

admin.py
--------
```
from django.contrib import admin
from Fakeapp.models import FakeData

@admin.register(FakeData)
class FakeAdmin(admin.ModelAdmin):
    list_filter = ['first_name','job','company']
```

Database
--------
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'fakeapp',
        'USER': 'root',
        'PASSWORD':'root'
    }
}
```


urls.py :
-------

```
from django.contrib import admin
from django.urls import path
from Fakeapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('fake/',views.fake_view)
]
```

### Create a superuser using 
```bash
python manage.py createsuperuser
```

or

```bash
winpty python manage.py createsuperuser
```

### Run the server using the command

```bash
python manage.py runserver
```

## ALL GOOD
